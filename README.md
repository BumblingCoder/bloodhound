# Bloodhound

Bloodhound aims to be a tracker style DAW with influences from Buzz, Psycle, and Reaper, as well as having robust handling for audio tracks. It may also eventually have a piano roll view.

Right now it does none of these things. This project may eventually be interesting, but right now it is only interesting to those interested in developing it.
