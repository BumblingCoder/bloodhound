import qbs

Project {
    qbsSearchPaths: "qbs"
    references: ["src/ui/ui.qbs", "src/audiocore/audiocore.qbs", "test/audiocore/audiocore-test.qbs", "qbs/juce_libraries/juce_libraries.qbs"]

    Project {
        AutotestRunner {
            name: "run-tests"
        }
    }
}
