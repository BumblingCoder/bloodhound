import qbs

StaticLibrary {
    property string basePath: project.sourceDirectory + "/external/JUCE"
    property string moduleName
    property string modulesBasePath: basePath + "/modules/"
    property string modulePath: modulesBasePath + moduleName
    property var extraDefines: []

    Depends {
        name: "cpp"
    }
    cpp.defines: ["JUCE_GLOBAL_MODULE_SETTINGS_INCLUDED"].concat(extraDefines)
    cpp.includePaths: [modulesBasePath]
    files: [modulePath + "/*"]
    excludeFiles: [modulePath + "/*.mm"]

    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: [product.modulesBasePath, product.modulePath]
        cpp.defines: product.cpp.defines
    }
}
