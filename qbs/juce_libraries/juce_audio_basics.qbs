import qbs

JuceModule {
    moduleName: "juce_audio_basics"
    Depends {
        name: "juce_core"
    }
    //    Depends {
    //        name: "xest"
    //    }
    Depends {
        name: "x11"
    }
    Depends {
        name: "xinerama"
    }
}
