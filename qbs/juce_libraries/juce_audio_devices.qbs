import qbs

JuceModule {
    moduleName: "juce_audio_devices"
    extraDefines: ["JUCE_JACK=1"]
    Depends {
        name: "juce_audio_basics"
    }
    Depends {
        name: "juce_events"
    }
}
