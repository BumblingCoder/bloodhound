import qbs

JuceModule {
    moduleName: "juce_audio_formats"
    Depends {
        name: "juce_audio_basics"
    }
    Depends {
        name: "flac"
    }
    Depends {
        name: "vorbis"
    }
    Depends {
        name: "vorbisenc"
    }
    Depends {
        name: "vorbisfile"
    }
}
