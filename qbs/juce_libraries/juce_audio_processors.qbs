import qbs

JuceModule {
    moduleName: "juce_audio_processors"
    Depends {
        name: "juce_gui_extra"
    }
    Depends {
        name: "juce_audio_basics"
    }
}
