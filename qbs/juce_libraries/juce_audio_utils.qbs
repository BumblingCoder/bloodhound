import qbs

JuceModule {
    moduleName: "juce_audio_utils"
    Depends {
        name: "juce_gui_extra"
    }
    Depends {
        name: "juce_audio_processors"
    }
    Depends {
        name: "juce_audio_formats"
    }
    Depends {
        name: "juce_audio_devices"
    }
}
