import qbs

JuceModule {
    moduleName: "juce_core"
    extraDefines: ["JUCE_STANDALONE_APPLICATION"]
}
