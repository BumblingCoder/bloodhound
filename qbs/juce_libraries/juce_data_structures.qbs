import qbs

JuceModule {
    moduleName: "juce_data_structures"
    Depends {
        name: "juce_events"
    }
}
