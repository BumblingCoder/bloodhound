import qbs

JuceModule {
    moduleName: "juce_dsp"
    Depends {
        name: "juce_audio_basics"
    }
    Depends {
        name: "juce_audio_formats"
    }
}
