import qbs

JuceModule {
    moduleName: "juce_events"
    Depends {
        name: "juce_core"
    }
}
