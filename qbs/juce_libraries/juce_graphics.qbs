import qbs

JuceModule {
    moduleName: "juce_graphics"
    Depends {
        name: "juce_events"
    }
    Depends {
        name: "freetype2"
    }
    extraDefines: ["JUCE_WEB_BROWSER=0"]
}
