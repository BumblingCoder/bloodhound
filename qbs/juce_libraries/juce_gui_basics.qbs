import qbs

JuceModule {
    moduleName: "juce_gui_basics"
    Depends {
        name: "juce_graphics"
    }
    Depends {
        name: "juce_data_structures"
    }
    Depends {
        name: "xext"
    }
}
