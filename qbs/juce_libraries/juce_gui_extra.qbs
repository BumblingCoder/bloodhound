import qbs

JuceModule {
    moduleName: "juce_gui_extra"
    Depends {
        name: "juce_gui_basics"
    }
}
