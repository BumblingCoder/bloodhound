import qbs

JuceModule {
    moduleName: "juce_osc"
    Depends {
        name: "juce_core"
    }
    Depends {
        name: "juce_events"
    }
}
