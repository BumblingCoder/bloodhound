import qbs

JuceModule {
    moduleName: "tracktion_engine"
    basePath: project.sourceDirectory + "/external/tracktion_engine"
    Depends {
        name: "juce_audio_basics"
    }
    Depends {
        name: "juce_audio_devices"
    }
    Depends {
        name: "juce_audio_utils"
    }
    Depends {
        name: "juce_gui_extra"
    }
    Depends {
        name: "juce_dsp"
    }
    Depends {
        name: "juce_osc"
    }
}
