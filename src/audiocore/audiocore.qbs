StaticLibrary {
    Depends {
        name: "juce_audio_basics"
    }
    Depends {
        name: "juce_audio_devices"
    }
    Depends {
        name: "juce_audio_formats"
    }
    Depends {
        name: "juce_events"
    }
    Depends {
        name: "juce_core"
    }
    Depends {
        name: "tracktion_engine"
    }

    Depends {
        name: "Qt.core"
    }

    Export {
        Depends {
            name: "cpp"
        }
        cpp.includePaths: product.cpp.includePaths
        cpp.defines: product.cpp.defines
    }

    cpp.cxxLanguageVersion: "c++17"
    cpp.includePaths: product.sourceDirectory

    cpp.defines: ["QT_DEPRECATED_WARNINGS"]
    cpp.dynamicLibraries: ["z", "dl", "curl", "asound"]

    files: ["*"]
}
