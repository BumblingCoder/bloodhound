#include "writewavefile.h"

#include <memory>

void bloodhound::audiocore::WriteWaveFile(const juce::AudioSampleBuffer& buffer,
                                          double sampleRate,
                                          QString filename) {
  juce::WavAudioFormat waveFormat;
  juce::File file(filename.toUtf8().data());
  if(file.exists()) { file.deleteFile(); }
  juce::FileOutputStream os(file);
  auto sampleRates = waveFormat.getPossibleSampleRates();
  auto writer = std::unique_ptr<juce::AudioFormatWriter>(waveFormat.createWriterFor(
      new juce::FileOutputStream(file), sampleRate, buffer.getNumChannels(), 16, false, 0));
  writer->writeFromAudioSampleBuffer(buffer, 0, buffer.getNumSamples());
}

juce::AudioSampleBuffer bloodhound::audiocore::ReadWaveFile(QString filename) {
  juce::WavAudioFormat waveFormat;
  juce::File file(filename.toUtf8().data());
  juce::FileInputStream is(file);
  auto reader = std::unique_ptr<juce::AudioFormatReader>(
      waveFormat.createReaderFor(new juce::FileInputStream(file), true));
  juce::AudioSampleBuffer buffer(reader->numChannels, reader->lengthInSamples);
  reader->read(&buffer, 0, reader->lengthInSamples, 0, true, true);
  return buffer;
}
