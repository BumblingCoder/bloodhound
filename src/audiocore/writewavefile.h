#pragma once

#include <QString>
#include <juce_audio_basics/juce_audio_basics.h>
#include <juce_audio_formats/juce_audio_formats.h>

namespace bloodhound::audiocore {
void WriteWaveFile(const juce::AudioSampleBuffer& buffer, double sampleRate, QString filename);

juce::AudioSampleBuffer ReadWaveFile(QString filename);
} // namespace bloodhound::audiocore
