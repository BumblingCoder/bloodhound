#include <QApplication>
#include <QIcon>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickStyle>

#include <juce_audio_devices/juce_audio_devices.h>

#include "document.h"
#include "mainactions.h"

int main(int argc, char* argv[]) {
  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

  QApplication app(argc, argv);

  QQuickStyle::setStyle("org.kde.desktop");

  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("Document", new Document(nullptr));
  engine.rootContext()->setContextProperty("Actions", new MainActions(nullptr));
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine,
      &QQmlApplicationEngine::objectCreated,
      &app,
      [url](QObject* obj, const QUrl& objUrl) {
        if(!obj && url == objUrl) QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
