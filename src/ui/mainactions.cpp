#include "mainwindow.h"

#include <QFileDialog>
#include <QString>
#include <QToolBar>

#include <QtDebug>
MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent), _engine(std::make_unique<tracktion_engine::Engine>("Bloodhound")) {

  auto toolbar = new QToolBar(this);
  addToolBar(Qt::ToolBarArea::TopToolBarArea, toolbar);
  toolbar->setMovable(false);

  auto open = new QAction(this);
  open->setText(tr("Open"));
  open->setIcon(QIcon::fromTheme("document-open"));
  open->setShortcut(QKeySequence(tr("Ctrl+O", "Open Action")));

  connect(open, &QAction::triggered, this, &MainWindow::openTriggered);

  toolbar->addAction(open);
}

void MainWindow::openTriggered() {
  auto filename =
      QFileDialog::getOpenFileName(this, tr("Open File"), {}, "Tracktion Files (*.tracktionedit)");
  if(!QFile::exists(filename)) return;

  _edit = std::make_unique<tracktion_engine::Edit>(
      *_engine,
      tracktion_engine::loadEditFromFile(*_engine, juce::File(filename.toStdString()), {}),
      tracktion_engine::Edit::EditRole::forEditing,
      nullptr,
      0);
  qDebug() << "Edit name " << _edit->getName().toRawUTF8();
  qDebug() << "Edit length " << _edit->getLength();
}
