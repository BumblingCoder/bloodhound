#pragma once

#include <QMainWindow>

#include <tracktion_engine.h>

#include <memory>

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget* parent = nullptr);
  ~MainWindow() = default;

private:
  void openTriggered();

private:
  std::unique_ptr<tracktion_engine::Engine> _engine;
  std::unique_ptr<tracktion_engine::Edit> _edit;
};
