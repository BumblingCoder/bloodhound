import QtQuick 2.14
import QtQuick.Controls 2.14

ApplicationWindow {
    header: ToolBar {
        ToolButton {
            text: qsTr("Open")
            icon.name: "document-open"
            onClicked: {
                Document.open(Actions.chooseProjectFile())
            }
        }
    }
    visible: true
    title: qsTr("Hello World")
}
