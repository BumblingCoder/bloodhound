Application {
    name: "bloodhound"
    Depends {
        name: "Qt.quick"
    }
    Depends {
        name: "Qt.widgets"
    }

    Depends {
        name: "Qt.quickcontrols2"
    }
    Depends {
        name: "audiocore"
    }

    Group {
        name: "qml"
        fileTags: "qt.core.resource_data"
        files: ["qml/**"]
    }

    // Additional import path used to resolve QML modules in Qt Creator's code model
    property pathList qmlImportPaths: []

    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: ["QT_DEPRECATED_WARNINGS"]

    files: ["*"]

    Group {
        // Properties for the produced executable
        fileTagsFilter: "application"
        qbs.install: true
        qbs.installDir: "bin"
    }
}
