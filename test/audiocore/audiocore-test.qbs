CppApplication {
    name: "audiocore-test"
    Depends {
        name: "audiocore"
    }
    Depends {
        name: "gtest_main"
    }
    Depends {
        name: "Qt.core"
    }

    cpp.cxxLanguageVersion: "c++17"
    cpp.defines: ["QT_DEPRECATED_WARNINGS", "TEST_DATA_DIR=\""
        + product.sourceDirectory + "/data/\""]
    type: base.concat("autotest")
    files: ["*"]
}
