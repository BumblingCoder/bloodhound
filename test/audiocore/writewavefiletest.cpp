#include <gtest/gtest.h>

#include <QByteArray>
#include <QFile>
#include <array>
#include <juce_audio_basics/juce_audio_basics.h>

#include "writewavefile.h"

static constexpr std::array<float, 20> KnownSamplesFloat = {0,
                                                            0.9903564453125,
                                                            -0.274261474609375,
                                                            -0.9144287109375,
                                                            0.52752685546875,
                                                            0.768310546875,
                                                            -0.740264892578125,
                                                            -0.5633544921875,
                                                            0.896331787109375,
                                                            0.315032958984375,
                                                            -0.98345947265625,
                                                            -0.042816162109375,
                                                            0.995452880859375,
                                                            -0.23297119140625,
                                                            -0.93084716796875,
                                                            0.49072265625,
                                                            0.794921875,
                                                            -0.710784912109375,
                                                            -0.598236083984375,
                                                            0.87664794921875};

static void CompareWavFiles(QString wav1, QString wav2) {
  QFile outputFile(wav1);
  outputFile.open(QFile::ReadOnly);
  auto outputBytes = outputFile.readAll();
  QFile expectedFile(wav2);
  expectedFile.open(QFile::ReadOnly);
  auto expectedBytes = expectedFile.readAll();
  // JUCE writes more into the header than audacity, so we skip to the data section.
  outputBytes = outputBytes.mid(outputBytes.indexOf("data"));
  expectedBytes = expectedBytes.mid(expectedBytes.indexOf("data"));
  ASSERT_EQ(expectedBytes.size(), outputBytes.size());
  for(int i = 0; i < outputBytes.size(); ++i) {
    EXPECT_NEAR(outputBytes[i], expectedBytes[i], 1) << i;
  }
}

TEST(WriteWaveFile, WaveFileMatchesExpected) {
  juce::AudioSampleBuffer buffer(1, 20);
  for(size_t i = 0; i < KnownSamplesFloat.size(); ++i) {
    buffer.setSample(0, i, KnownSamplesFloat[i]);
  }
  for(int i = 0; i < buffer.getNumSamples(); ++i) {
    ASSERT_EQ(buffer.getSample(0, i), KnownSamplesFloat[i]) << i;
  }
  bloodhound::audiocore::WriteWaveFile(buffer, 44100, "test.wav");
  CompareWavFiles("test.wav", TEST_DATA_DIR "untitled.wav");
}

TEST(WriteWaveFile, WrittenWavMatchesExpected) {
  auto buffer = bloodhound::audiocore::ReadWaveFile(TEST_DATA_DIR "untitled.wav");
  for(int i = 0; i < buffer.getNumSamples(); ++i) {
    EXPECT_FLOAT_EQ(buffer.getSample(0, i), KnownSamplesFloat[i]) << i;
  }
}

TEST(WriteWaveFile, ReadWriteCompare) {
  auto buffer = bloodhound::audiocore::ReadWaveFile(TEST_DATA_DIR "untitled.wav");
  bloodhound::audiocore::WriteWaveFile(buffer, 44100, "test.wav");
  CompareWavFiles("test.wav", TEST_DATA_DIR "untitled.wav");
}
